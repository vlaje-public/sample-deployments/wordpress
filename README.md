# WordPress

[toc]

## Deploy

```bash
git clone git@gitlab.com:013_tmpl/wordpress.git && cd wordpress
```

- For internet-exposed deployments, **set stronger DB password**
- For behind-Traefik deployment, adjust the **exposed port to 8080**

```bash
mcedit compose.yml
```

```bash
docker compose up -d
```

### Increase PHP Limits

```bash
docker exec -it wp bash
```

```bash
cd /usr/local/etc/php/ \
&& cp php.ini-development php.ini \
&& sed -i 's/memory_limit = 128M/memory_limit = 1024M/g' php.ini \
&& sed -i 's/max_execution_time = 30/max_execution_time = 360/g' php.ini
```

Quit the container

```bash
docker restart wp
```

## Duplicator - Migrate To Container

```bash
sudo su
```

### VBox

Have the default WP running

Copy the backup to the VM's "Downloads"

```bash
cd /var/lib/docker/volumes/wp/_data/ && rm -rf * \
&& cp /home/test/Downloads/*.zip . \
&& cp /home/test/Downloads/installer.php . \
&& chown www-data:www-data *
```

### AWS

Have the default WP running

Copy the backup to the VM's home

```bash
cd /var/lib/docker/volumes/wp/_data/ && rm -rf * \
&& cp /home/admin/*.zip . \
&& cp /home/admin/installer.php . \
&& chown www-data:www-data * && cd
```

Choose the **"Advaced"** mode of migration

```
http://localhost/installer.php
```

### DB

- Host
    - db
- DB name
    - db
- DB user
    - user
- DB password
    - pass

- "Step 2" of the migration wizard
  - **Advanced**
    - Collation: **utf8mb4_czech_ci**

### Disable WordFence (And Other Plugins)

Either in **"Step 3"** of the Duplicator migration wizard, or

```bash
cd /var/lib/docker/volumes/wp/_data/wp-content/plugins/ \
&& mv wordfence/ _wordfence/ \
&& mv wordfence-login-security/ _wordfence-login-security/
```

### Fix Redirect Loop If Behind Traefik

```bash
mcedit /var/lib/docker/volumes/wp/_data/wp-config.php
```

Add to the top of wp-config.php

```php
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
  $_SERVER['HTTPS']='on';
```

**OR** - WIP

```php
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
  $_SERVER['HTTPS'] = 'on';
}
```

#### Sources

- https://gist.github.com/moinkhanif/a3665509c8d3648136a8d9508690d581
- https://wordpress.stackexchange.com/a/250254
- https://stackoverflow.com/questions/59931256/too-many-redirects-with-reverse-proxy

### Maintenance Mode

```
Maintenance Mode
```

```
Lukas Juhas
```

https://wordpress.org/plugins/lj-maintenance-mode/

```
ShortCode Redirect
```

(Cartpauj)

https://wordpress.org/plugins/shortcode-redirect/

```
wp-admin/options-general.php?page=lj-maintenance-mode
```

```html
<h1 style="text-align: left;">Log In</h1>
<p style="text-align: left;">[redirect url='/wp-admin' sec='0']</p>
```

### Back Up After Migration State

- Updraft
  - Reconfigure backup settings
  - Delete old backup records
  - Create a new backup

```bash
sudo su
```

AWS

```bash
cp -r /var/lib/docker/volumes/wp/_data/wp-content/updraft/ /home/admin/
```

## Sources/Links

### WP

- https://hub.docker.com/_/wordpress
- https://github.com/docker-library/wordpress/tree/master

### MariaDB

- https://hub.docker.com/_/mariadb
- https://mariadb.com/kb/en/mariadb-server-docker-official-image-environment-variables/

## ---

## 001_Arch

### wp-config.php

```bash
mcedit /var/lib/docker/volumes/wp/_data/wp-config.php
```

```plain
define('WP_MEMORY_LIMIT', '1024M');
set_time_limit(180);
```

### .htaccess

Also, edititng `.htaccess` has some effect but `wp-config.php` seems to be enough

```bash
mcedit /var/lib/docker/volumes/wp/_data/.htaccess
```

```plain
php_value memory_limit 1024M
php_value max_execution_time 180
```

### Helper Commands

```bash
docker exec -it wp bash
```

```bash
apt update && apt install -y mc \
&& cd /usr/local/etc/php/ \
&& cp php.ini-development php.ini \
&& mcedit php.ini
```

```bash
php -i | grep limit
```

---

```bash
rm -rf /var/lib/docker/volumes/wp/_data/*
```

```bash
cp /home/test/Downloads/* /var/lib/docker/volumes/wp/_data/
```

```bash
chown www-data:www-data /var/lib/docker/volumes/wp/_data/*
```

```bash
mv /var/lib/docker/volumes/wp/_data/wp-content/plugins/wordfence-login-security/ /var/lib/docker/volumes/wp/_data/wp-content/plugins/_wordfence-login-security/
```

```bash
mv /var/lib/docker/volumes/wp/_data/wp-content/plugins/wordfence/ /var/lib/docker/volumes/wp/_data/wp-content/plugins/_wordfence/
```
